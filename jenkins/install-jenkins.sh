#!/bin/bash

test "${EUID}" -ne 0 && printf "%s\n" "run as root" && exit 1

if ! command -v apt-get &>/dev/null; then
    printf "%s\n" "[INFO] distro is not debian-based"
    exit 1
fi

export DEBIAN_FRONTEND=noninteractive

apt-get update
apt-get install -y curl gpg openssl

curl -sSL https://pkg.jenkins.io/debian-stable/jenkins.io-2023.key | gpg --dearmor -o /usr/share/keyrings/jenkins-keyring.gpg
printf "%s\n" "deb [signed-by=/usr/share/keyrings/jenkins-keyring.gpg] https://pkg.jenkins.io/debian-stable binary/" > /etc/apt/sources.list.d/jenkins.list

apt-get update
apt-get install -y openjdk-17-jre-headless jenkins nginx

sed -i 's/#Environment="JENKINS_LISTEN_ADDRESS="/Environment="JENKINS_LISTEN_ADDRESS=127.0.0.1"/g' /usr/lib/systemd/system/jenkins.service

rm -rf /etc/nginx/sites-available /etc/nginx/sites-enabled
mkdir -p /etc/nginx/ssl &>/dev/null
cat > /etc/nginx/nginx.conf << EOF
user www-data;
worker_processes auto;
pid /run/nginx.pid;
error_log /var/log/nginx/error.log;
include /etc/nginx/modules-enabled/*.conf;

events {
    worker_connections 1024;
}
http {
    log_format  main  '\$remote_addr - \$remote_user [\$time_local] "\$request" '
                      '\$status \$body_bytes_sent "\$http_referer" '
                      '"\$http_user_agent" "\$http_x_forwarded_for"';

    access_log  /var/log/nginx/access.log  main;

    sendfile            on;
    tcp_nopush          on;
    keepalive_timeout   65;
    types_hash_max_size 4096;

    include             /etc/nginx/mime.types;
    default_type        application/octet-stream;

    upstream jenkins {
      keepalive 32;
      server 127.0.0.1:8080;
    }
    
    map \$http_upgrade \$connection_upgrade {
        default upgrade;
        '' close;
    }
    
    server {
        listen          443 ssl;
    
        server_name     $(hostname);
    
        ssl_certificate /etc/nginx/ssl/jenkins.crt;
        ssl_certificate_key /etc/nginx/ssl/jenkins.key;
    
        root            /var/cache/jenkins/war/;
    
        access_log      /var/log/nginx/jenkins.access.log;
        error_log       /var/log/nginx/jenkins.error.log;
    
        ignore_invalid_headers off;
    
        location ~ "^/static/[0-9a-fA-F]{8}\/(.*)\$" {
            rewrite "^/static/[0-9a-fA-F]{8}\/(.*)" /\$1 last;
        }
    
        location /userContent {
            root /var/lib/jenkins/;
            if (!-f \$request_filename){
                rewrite (.*) /\$1 last;
                break;
            }
            sendfile on;
        }

        location / {
            sendfile off;
            proxy_pass         http://jenkins;
            proxy_redirect     default;
            proxy_http_version 1.1;
    
            proxy_set_header   Connection        \$connection_upgrade;
            proxy_set_header   Upgrade           \$http_upgrade;
    
            proxy_set_header   Host              \$host;
            proxy_set_header   X-Real-IP         \$remote_addr;
            proxy_set_header   X-Forwarded-For   \$proxy_add_x_forwarded_for;
            proxy_set_header   X-Forwarded-Proto \$scheme;
            proxy_max_temp_file_size 0;
    
            client_max_body_size       2048m;
            client_body_buffer_size    128k;
    
            proxy_connect_timeout      90;
            proxy_send_timeout         90;
            proxy_read_timeout         90;
            proxy_buffering            off;
            proxy_request_buffering    off;
            proxy_set_header Connection "";
        }
    }
}
EOF

(
    cd /etc/nginx/ssl
    openssl req \
        -x509 \
        -newkey rsa:4096 \
        -keyout jenkins.key \
        -out jenkins.crt \
        -sha256 \
        -days 3650 \
        -nodes \
        -subj "/C=AU/ST=StateName/L=/O=Internet Widgits Pty Ltd/OU=/CN="
)
