#!/bin/bash

test "${EUID}" -ne 0 && printf "%s\n" "run as root" && exit 1

function usage() {
    printf "%s\n" \
        "download and configure jenkins node as systemd service" \
        "usage: $(basename $0) -u <http(s)://jenkins:port> -s <secret> -n <node name>"

    exit 1
}

if ! command -v apt-get &>/dev/null; then
    printf "%s\n" "[INFO] distro is not debian-based"
    exit 1
fi

while getopts "u:s:n:h" opts; do
    case "${opts}" in
        u) url="${OPTARG}";;
        s) secret="${OPTARG}";;
        n) name="${OPTARG}";;
        h) usage;;
    esac
done

USERNAME="jenkins"

if test "${url}" && test "${secret}" && test "${name}"; then
    DEBIAN_FRONTEND=noninteractive apt-get update
    DEBIAN_FRONTEND=noninteractive apt-get install -y vim sudo

    if ! command -v java &>/dev/null; then
        DEBIAN_FRONTEND=noninteractive apt-get install -y openjdk-17-jre-headless
    fi

    if ! grep -qi "${USERNAME}" /etc/passwd; then
        password=$(openssl rand -hex 32)
        useradd -m -s /bin/bash "${USERNAME}"
        printf "%s\n" "${USERNAME}:${password}" \
            | tee "/home/${USERNAME}/password.txt" \
            | chpasswd
        chown "${USERNAME}:${USERNAME}" "/home/${USERNAME}/password.txt"

        usermod -aG sudo "${USERNAME}"
        mkdir -p /etc/sudoers.d &>/dev/null
        printf "%s\n" "${USERNAME} ALL=(ALL) NOPASSWD:ALL" > "/etc/sudoers.d/${USERNAME}"
        chmod 0440 "/etc/sudoers.d/${USERNAME}"
        
        printf "%s\n" "[INFO] created jenkins user with NOPASSWD:ALL sudo"
    fi

    if curl -sSkL "${url}/jnlpJars/agent.jar" -o "/home/${USERNAME}/agent.jar"; then
        printf "%s\n" "[INFO] downloaded ${url}/jnlpJars/agent.jar to /home/${USERNAME}/agent.jar"
    else
        printf "%s\n" "[ERR] failed to download jenkins agent jar: ${url}/jnlpJars/agent.jar"
        exit 1
    fi

    cat > /etc/systemd/system/jenkins-agent.service << EOF
[Unit]
Description=Jenkins Agent - ${name}
After=network.target
Requires=network.target
StartLimitIntervalSec=500
StartLimitBurst=5

[Service]
Type=simple
ExecStart=$(which java) -jar /home/${USERNAME}/agent.jar -url ${url} -secret ${secret} -name ${name} -webSocket -workDir "/home/${USERNAME}" -noCertificateCheck
Restart=on-failure
User=${USERNAME}
RestartSec=20
ExecStartPre=/bin/sleep 180

[Install]
WantedBy=multi-user.target
EOF

    printf "%s\n" \
        "[INFO] successfully created jenkins node systemd service" \
        "[INFO] enable and start the agent systemd service: jenkins-agent.service"
else
    printf "%s\n" "[ERR] missing -u|-s|-n|-w"
    usage
fi
