#!/bin/bash

shopt -s nullglob
shopt -s extglob
declare -A latest_date
declare -A latest_ext

function usage() {
    printf "%s\n" \
        "automation script to rsync latest vm builds from source to dest" \
        "removes non-latest builds from source and from dest" \
        "destination must be a usb mountpoint external storage" \
        "" \
        "usage: $(basename ${0}) -s <source dir> -d <dest dir>"
    exit 1
}

get_mount_point() {
    local path="${1}"
    test -z "${path}" && return 1
    path=$(cd "${path}" 2>/dev/null && pwd || printf "%s\n" "${path}")
    while [[ "${path}" != "/" ]]; do
        if grep -q "[[:space:]]${path}[[:space:]]" /proc/mounts; then
            printf "%s\n" "$path"
            return 0
        fi
        path="${path%/*}"
        test -z "${path}" && return 1
    done
    return 1
}

while getopts "s:d:h" opts; do
    case "${opts}" in
        s) source="${OPTARG}";;
        d) dest="${OPTARG}";;
        h) usage;;
        *) usage;;
    esac
done

source="${source%%+(/)}"
dest="${dest%%+(/)}"

if ! test "${source}" || ! test "${dest}"; then
    usage
fi

mkdir -p "${dest}" &>/dev/null

mount_point=$(get_mount_point "${dest}")
if test -z "${mount_point}"; then
    printf "%s\n" "[ERR] external storage directory ${dest} is not a mounted volume"
    exit 1
fi

for file in "${source}"/*.{ova,qcow2}; do
    test -e "${file}" || continue
    filename=$(basename "${file}")

    if [[ "${filename}" =~ ^(.+)_([0-9]{4}-[0-9]{2}-[0-9]{2})\.([^.]+)$ ]]; then
        identifier="${BASH_REMATCH[1]}"
        file_date="${BASH_REMATCH[2]}"
        file_ext="${BASH_REMATCH[3]}"

        if test -z "${latest_date[${identifier}]}" || test "${file_date}" \> "${latest_date[${identifier}]}"; then
            latest_date["${identifier}"]="${file_date}"
            latest_ext["${identifier}"]="${file_ext}"
        fi
    else
        printf "%s\n" "[WARN] file \"${filename}\" does not match expected pattern, skipping"
    fi
done

printf "%s\n" "[INFO] latest builds identified:"
for id in "${!latest_date[@]}"; do
    printf "%s\n" \
        "  - identifier: ${id}" \
        "  - date: ${latest_date[${id}]}" \
        "  - extension: ${latest_ext[${id}]}" \
        ""
done

for id in "${!latest_date[@]}"; do
    file_date="${latest_date[${id}]}"
    ext="${latest_ext[${id}]}"
    main_file="${source}/${id}_${file_date}.${ext}"
    sums_file="${source}/${id}_${file_date}.${ext}.sums"
    printf "%s\n" "[INFO] syncing latest build for \"${id}\""

    if test -f "${main_file}"; then
        rsync -av "${main_file}" "${dest}/"
    else
        printf "%s\n" "[WARN] expected main file \"${main_file}\" not found"
    fi

    if test -f "${sums_file}"; then
        rsync -av "${sums_file}" "${dest}/"
    else
        printf "%s\n" "[WARN] expected sums file \"${sums_file}\" not found"
    fi
done

printf "%s\n" "[INFO] reclaiming (deleting) older builds from ${source}"
for file in "${source}"/*.{ova,qcow2}; do
    test -e "${file}" || continue
    filename=$(basename "${file}")

    if [[ "${filename}" =~ ^(.+)_([0-9]{4}-[0-9]{2}-[0-9]{2})\.([^.]+)$ ]]; then
        identifier="${BASH_REMATCH[1]}"
        file_date="${BASH_REMATCH[2]}"
        file_ext="${BASH_REMATCH[3]}"

        if test "${file_date}" != "${latest_date[${identifier}]}"; then
            printf "%s\n" "[INFO] removing older file: ${file}"
            rm -f "${file}"
            corresponding_sums="${source}/${identifier}_${file_date}.${file_ext}.sums"

            if test -f "${corresponding_sums}"; then
                printf "%s\n" "[INFO] removing older sums file: ${corresponding_sums}"
                rm -f "${corresponding_sums}"
            fi
        fi
    else
        printf "%s\n" "[WARN] skipping file \"${filename}\" for deletion (pattern not matched)"
    fi
done

printf "%s\n" "[INFO] reclaiming (deleting) older builds from ${dest}"
for file in "${dest}"/*.{ova,qcow2}; do
    test -e "${file}" || continue
    filename=$(basename "${file}")

    if [[ "${filename}" =~ ^(.+)_([0-9]{4}-[0-9]{2}-[0-9]{2})\.([^.]+)$ ]]; then
        identifier="${BASH_REMATCH[1]}"
        file_date="${BASH_REMATCH[2]}"
        file_ext="${BASH_REMATCH[3]}"

        if test -n "${latest_date[${identifier}]}" && test "${file_date}" != "${latest_date[${identifier}]}"; then
            printf "%s\n" "[INFO] removing older file from ${dest}: ${file}"
            rm -f "${file}"
            corresponding_sums="${dest}/${identifier}_${file_date}.${file_ext}.sums"
            if test -f "${corresponding_sums}"; then
                printf "%s\n" "[INFO] removing older sums file from ${dest}: ${corresponding_sums}"
                rm -f "${corresponding_sums}"
            fi

        elif test -z "${latest_date[${identifier}]}" ; then
            printf "%s\n" "[INFO] removing orphan file from ${dest}: ${file}"
            rm -f "${file}"
            corresponding_sums="${dest}/${identifier}_${file_date}.${file_ext}.sums"
            if test -f "${corresponding_sums}"; then
                printf "%s\n" "[INFO] removing orphan sums file from ${dest}: ${corresponding_sums}"
                rm -f "${corresponding_sums}"
            fi
        fi
    else
        printf "%s\n" "[WARN] skipping file \"${filename}\" in ${dest} for deletion (pattern not matched)"
    fi
done

printf "%s\n" "[INFO] sync and reclaim complete"
