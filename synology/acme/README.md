### install acme.sh

```
curl https://get.acme.sh | sh -s email=my@example.com
wget -O -  https://get.acme.sh | sh -s email=my@example.com
```

### issue/renew via dns namecheap challenge

```
export NAMECHEAP_USERNAME=""
export NAMECHEAP_API_KEY=""
export NAMECHEAP_SOURCEIP=""

# set default letsencrypt server
/var/services/homes/admin/.acme.sh/acme.sh --set-default-ca --server letsencrypt

# issue
/var/services/homes/admin/.acme.sh/acme.sh --issue --dns dns_namecheap -d example.com -d '*.example.com'

# optionall renew
/var/services/homes/admin/.acme.sh/acme.sh --renew--dns dns_namecheap -d example.com -d '*.example.com'
```

### renew cron and deploy on synology

```
/var/services/homes/admin/.acme.sh/acme.sh --cron --home /var/services/homes/admin/.acme.sh
/var/services/homes/admin/.acme.sh/acme.sh --deploy --home /var/services/homes/admin/.acme.sh -d homelan.mx --deploy-hook synology_dsm
```

### deploy on proxmox

```
export DEPLOY_PROXMOXVE_USER="root"
export DEPLOY_PROXMOXVE_USER_REALM="pam"
export DEPLOY_PROXMOXVE_API_TOKEN_NAME="name"
export DEPLOY_PROXMOXVE_API_TOKEN_KEY="uuid"
/var/services/homes/admin/.acme.sh/acme.sh --home /var/services/homes/admin/.acme.sh --deploy -d example.com --deploy-hook proxmoxve
```
