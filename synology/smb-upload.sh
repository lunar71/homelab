#!/bin/bash

if ! command -v smbclient &>/dev/null; then
    printf "%s\n" "[ERR] smbclient not found"
    exit 1
fi

usage() {
    printf "%s\n" \
        "upload directory recursively to smb server" \
        "usage: $(basename $0) -S <smb server> -D <smb share> -s <local src> -d <remote dst> -u <smb username>" \
        "" \
        "-S <smb server>    required: smb server" \
        "-D <smb share>     required: smb share name" \
        "-s <local src>     required: local source path" \
        "-d <remote dst>    required: remote smb destination path, relative to -D" \
        "-u <username>      required: smb username"
    exit 1
}

while getopts "S:D:s:d:u:h" opts; do
    case $opts in
        S) server="${OPTARG}";;
        D) share="${OPTARG}";;
        s) src="${OPTARG}";;
        d) dst="${OPTARG}";;
        u) username="${OPTARG}";;
        h) usage;;
    esac
done

if test "${server}" && test "${share}" && test "${src}" && test "${dst}" && test "${username}"; then
    src_dirname=$(dirname "${src}")
    src_basename=$(basename "${src}")
    smbclient \
        "//${server}/${share}" \
        -U "${username}" \
        -c "recurse on; prompt off; cd ${dst}; lcd ${src_dirname}; mput ${src_basename}"
else
    usage
fi
