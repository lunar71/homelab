#!/usr/bin/env python3

import os
import yt_dlp
import threading

from fastapi import FastAPI, Query, HTTPException
from fastapi.responses import JSONResponse

app = FastAPI()

DOWNLOAD_DIR = '/app/downloads'
os.makedirs(DOWNLOAD_DIR, exist_ok=True)

def download_audio_from_youtube(url: str, folder: str = None):
    ydl_opts = {
        'format': 'bestaudio/best',
        'outtmpl': f'{DOWNLOAD_DIR}/%(title)s.%(ext)s',
        'postprocessors': [{
            'key': 'FFmpegExtractAudio',
            'preferredcodec': 'mp3',
            'preferredquality': '320',
        }],
    }

    if folder:
        os.makedirs(os.path.join(DOWNLOAD_DIR, folder), exist_ok=True)
        ydl_opts['outtmpl'] = f'{DOWNLOAD_DIR}/{folder}/%(title)s.%(ext)s'

    try:
        with yt_dlp.YoutubeDL(ydl_opts) as ydl:
            info = ydl.extract_info(url, download=True)
            downloaded_filename = ydl.prepare_filename(info)
            mp3_filename = downloaded_filename.rsplit('.', 1)[0] + '.mp3'
            return mp3_filename

    except Exception as e:
        raise HTTPException(status_code=400, detail=str(e))

def background_download(url: str, folder: str = None):
    try:
        download_audio_from_youtube(url, folder)
    except Exception as e:
        print(f'error in background download: {e}')

@app.get("/")
def index():
    return {"message": "hello from yt-dlp fastapi"}

@app.get('/status')
def status():
    return 'ok'

@app.get('/d/')
def download(url: str, folder: str = Query(None)):
    if not url:
        raise HTTPException(
            status_code=400,
            detail='missing video url'
        )

    download_thread = threading.Thread(
        target=background_download,
        args=(url,folder)
    )
    download_thread.start()

    return JSONResponse(
        status_code=200,
        content={
            'message': 'download started in the background',
            'url': url,
            'folder': folder or 'default'
        }
    )

if __name__ == '__main__':
    import uvicorn
    uvicorn.run(app, host='0.0.0.0', port=8000)

