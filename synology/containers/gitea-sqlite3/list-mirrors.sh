#!/bin/bash

usage() {
    printf "%s\n" \
        "usage: $(basename $0) -g <git server> -u <username> -t <token>"
    exit 1
}

if ! command -v curl &>/dev/null; then
    printf "%s\n" "[ERR] curl not found"
    exit 1
fi

while getopts "g:u:t:" opts; do
    case $opts in
        g) git_server="${OPTARG}";;
        u) username="${OPTARG}";;
        t) token="${OPTARG}";;
    esac
done

if test "${git_server}" && test "${username}" && test "${token}"; then
    curl \
        -H "Authorization: token ${token}" \
        "${git_server}/api/v1/users/${username}/repos?type=mirror" | jq
else
    usage
fi
