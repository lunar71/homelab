#!/bin/bash

SCRIPT_DIR=$(dirname "${BASH_SOURCE[0]}")
cd "${SCRIPT_DIR}" || exit 1

git config --global --add safe.directory '*'

printf "%s\n" \
    "## Mirrored repos" \
        "" > README.md

for i in *.git; do
    (
        cd $i
        git remote update && \
            printf "%s\n" \
            "[INFO] updated ${i}"

        printf -- "- %s\n" \
            "$(git remote get-url origin)" >> ../README.md
    )
done
