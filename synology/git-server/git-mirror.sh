#!/bin/bash

SCRIPT_DIR=$(dirname "${BASH_SOURCE[0]}")
cd "${SCRIPT_DIR}" || exit 1

if test "${1}"; then
    git clone --mirror $1
else
    printf "%s\n" "usage: $(basename $0) <git repo>"
    exit 1
fi
