#!/bin/bash

test "${EUID}" -ne 0 && printf "%s\n" "run as root" && exit 1

if ! command -v curl &>/dev/null; then
    printf "%s\n" "[ERR] curl not found"
    exit 1
fi

if ! command -v java &>/dev/null; then
    printf "%s\n" "[ERR] java not found"
    exit 1
fi

java_version=$(java -version 2>&1 | awk -F '"' '/version/ {print $2}')
if test "${java_version}" \< "17"; then
    printf "%s\n" "[ERR] Java 17 or higher required, found ${java_version}"
    exit 1
fi

if ! id minecraft; then
    adduser --system --group --no-create-home minecraft
fi

(
    mkdir -p /opt/minecraft-server &>/dev/null
    cd /opt/minecraft-server
    curl -sSfL https://piston-data.mojang.com/v1/objects/4707d00eb834b446575d89a61a11b5d548d8c001/server.jar -o server.jar
    printf "%s\n" "eula=true" > eula.txt
)

(
    mkdir -p /opt/minecraft-geyser &>/dev/null
    cd /opt/minecraft-geyser
    curl -sSfL https://download.geysermc.org/v2/projects/geyser/versions/latest/builds/latest/downloads/standalone -o geysermc.jar

    cat > config.yml << EOF
bedrock: 
   address: 0.0.0.0 
   port: 19132 

remote:
   address: auto
   port: 25565
   auth-type: online
EOF
)

chown minecraft:minecraft /opt/minecraft-*
chmod 0755 /opt/minecraft-*

cat > /etc/systemd/system/minecraft-server.service << EOF
[Unit]
Description=Minecraft Server
Wants=network-online.target
After=network-online.target

[Service]
User=minecraft
WorkingDirectory=/opt/minecraft-server
ExecStart=/usr/bin/java -Xmx2048M -Xms2048M -jar /opt/minecraft-server/server.jar nogui
Restart=always
RestartSec=30
# Restart=on-success
StandardInput=null

[Install]
WantedBy=multi-user.target
EOF

cat > /etc/systemd/system/geyser-mc.service << EOF
[Unit]
Description=Minecraft Geyser
Wants=network-online.target
After=network-online.target

[Service]
User=minecraft
WorkingDirectory=/opt/minecraft-geyser
ExecStart=/usr/bin/java -Xmx1024M -Xms1024M -jar /opt/minecraft-geyser/geysermc.jar
Restart=always
RestartSec=30
# Restart=on-success
StandardInput=null

[Install]
WantedBy=multi-user.target
EOF
