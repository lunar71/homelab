#!/bin/bash
set -e

LOCAL_IP=$(ip route get 8.8.8.8 | grep -oP 'src \K[^ ]+')
CA_DIR="/etc/certificate-authority"
CERTS_DIR="${CA_DIR}/certs"
OPENSSL_CONF="/etc/ssl/openssl.cnf"
DEPENDENCIES="openssl nginx qrencode"

test "${EUID}" -ne 0 && printf "%s\n" "run as root" && exit 1

if ! command -v apt-get &>/dev/null; then
    printf "%s\n" "[ERR] distribution not debian-based"
else
    for i in ${DEPENDENCIES}; do
        if ! command -v $i &>/dev/null; then
            DEBIAN_FRONTEND=noninteractive apt-get update
            DEBIAN_FRONTEND=noninteractive apt-get install -yq $i
        fi
    done
fi

if ! test "${1}"; then
    printf "%s\n" \
        "generate self signed ca and certificates" \
        "usage: $(basename $0) <name>"
    exit 1
else
    ROOT_CA_NAME="${1}"
fi

ROOT_CA_PEM="${CA_DIR}/${1}.root.pem"
ROOT_CA_CRT="${CA_DIR}/${1}.root.crt"
ROOT_CA_KEY="${CA_DIR}/${1}.root.key"

if test -d "${CA_DIR}"; then
    printf "%s\n" "[INFO] ${CA_DIR} already exists, removing"
    rm -rf "${CA_DIR}"
fi

mkdir -p "${CA_DIR}" "${CERTS_DIR}" /var/www/html/files /etc/nginx/ssl &>/dev/null

if openssl \
    req \
    -x509 \
    -newkey rsa:4096 \
    -keyout "${ROOT_CA_KEY}" \
    -out "${ROOT_CA_PEM}" \
    -sha256 \
    -days 3650 \
    -nodes \
    -subj "/CN=root.${ROOT_CA_NAME}" \
    -addext "subjectAltName = IP:${LOCAL_IP}, DNS:root.${1}" &>/dev/null; then
    printf "%s\n" "[INFO] root ca created at ${ROOT_CA_PEM}"
    openssl x509 -outform der -in "${ROOT_CA_PEM}" -out "${ROOT_CA_CRT}"
fi

cp /etc/certificate-authority/${ROOT_CA_NAME}.root.pem /var/www/html/files/root.pem
cp /etc/certificate-authority/${ROOT_CA_NAME}.root.crt /var/www/html/files/root.crt

if openssl \
    req -x509 \
    -newkey rsa:4096 \
    -nodes \
    -keyout "/etc/nginx/ssl/CA.${1}.key" \
    -out "/etc/nginx/ssl/CA.${1}.crt" \
    -sha256 \
    -days 3650 \
    -CA "${ROOT_CA_PEM}" \
    -CAkey "${ROOT_CA_KEY}" \
    -subj "/CN=CA.${1}" \
    -addext "subjectAltName = IP:${LOCAL_IP}, DNS:CA.${1}" &>/dev/null; then
    printf "%s\n" "[INFO] created certificate for CA.${1}"
fi


cat > /etc/nginx/nginx.conf << EOF
worker_processes auto;
error_log /dev/null;
pid /var/run/nginx.pid;

events {
    worker_connections 1024;
}

http {
    include       mime.types;
    default_type  application/octet-stream;
    access_log /dev/null;

    server {
        listen 80;
        server_name 192.168.133.46;
        
        return 301 https://\$host\$request_uri;
    }

    server {
        listen 443 ssl;
        server_name 192.168.133.46;

        ssl_certificate /etc/nginx/ssl/CA.${1}.crt;
        ssl_certificate_key /etc/nginx/ssl/CA.${1}.key;
        ssl_protocols TLSv1.2 TLSv1.3;
        ssl_ciphers HIGH:!aNULL:!MD5;

        root /var/www/html;
        index index.html index.htm;

        location / {
            try_files \$uri \$uri/ =404;
        }

        location = /root.pem {
            root /var/www/html/files;
            add_header Content-Disposition "attachment; filename=${1}.root.pem";
            try_files \$uri =404;
        }

        location = /root.crt {
            root /var/www/html/files;
            add_header Content-Disposition "attachment; filename=${1}.root.crt";
            try_files \$uri =404;
        }

        location = /install.sh {
            root /var/www/html/files;
            add_header Content-Disposition "attachment; filename=install.sh";
            try_files \$uri =404;
        }

        location = /install.ps1 {
            root /var/www/html/files;
            add_header Content-Disposition "attachment; filename=install.ps1";
            try_files \$uri =404;
        }
    }
}
EOF

rm -rf /var/www/html/*.*
cat > "/var/www/html/index.html" << EOF
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="data:,">
    <title>${1} Root CA</title>
    <style>
        body { font-family: Arial, sans-serif; text-align: center; padding: 50px; }
        a { display: block; margin: 10px 0; font-size: 18px; }
    </style>
</head>
<body>
    <h2>${1} Root CA Downloads</h2>
    <p><a href="/root.pem">Download Root Certificate</a></p>
    <p><pre>curl -fsSkL https://${LOCAL_IP}/root.pem</pre></p>
    <p>
<pre>
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
[Net.ServicePointManager]::ServerCertificateValidationCallback = { \$true }
(new-object system.net.webclient).downloadfile("https://${LOCAL_IP}/root.crt", "C:\\root.${1}.crt")
</pre>
    </p>
    <p><a href="/install.sh">Download Linux Install Script</a></p>
    <p><pre>curl -fsSkL https://192.168.133.46/install.sh | sudo bash</pre></p>
    <p><a href="/install.ps1">Download Windows Install Script</a></p>
    <p>
<pre>
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
[Net.ServicePointManager]::ServerCertificateValidationCallback = { \$true }
iex (new-object system.net.webclient).downloadstring("https://192.168.133.46/install.ps1")
</pre>
    </p>
    <h2>Scan the QR code to import the Root CA via URL</h2>
    <img src="files/${LOCAL_IP}-rootCA.png">
</body>
</html>
EOF

cat > /etc/certificate-authority/install.sh << EOF
#!/bin/bash
set -e

test "\${EUID}" -ne 0 && printf "%s\n" "run as root" && exit 1

CA_URL="https://${LOCAL_IP}"
ROOT_CERT_NAME="${1}.crt"
LINUX_CA_PATH="/usr/local/share/ca-certificates"
ROOT_CA_FULL_PATH="\${LINUX_CA_PATH}/\${ROOT_CERT_NAME}"

if ! command -v apt-get &>/dev/null; then
    printf "%s\\n" "[ERR] distribution not debian-based"
    exit 1
else
    DEBIAN_FRONTEND=noninteractive apt-get update
    DEBIAN_FRONTEND=noninteractive apt-get install -yq p11-kit p11-kit-modules
fi

if curl -sSkL "\${CA_URL}/root.pem" -o "\${ROOT_CA_FULL_PATH}"; then
    printf "%s\\n" "[INFO] downloaded root certificate from \${CA_URL} to \${ROOT_CA_FULL_PATH}"
fi

if update-ca-certificates; then
    printf "%s\\n" "[INFO] step-ca root certificate installed successfully"
fi

cp /usr/lib/x86_64-linux-gnu/nss/libnssckbi.so /usr/lib/x86_64-linux-gnu/nss/libnssckbi.so.bak

ln -sf \
    /usr/lib/x86_64-linux-gnu/pkcs11/p11-kit-trust.so \
    /usr/lib/x86_64-linux-gnu/nss/libnssckbi.so
EOF
chmod +x /etc/certificate-authority/install.sh

cat > /etc/certificate-authority/install.ps1 << EOF
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
[Net.ServicePointManager]::ServerCertificateValidationCallback = { \$true }

\$ErrorActionPreference = "Stop"
\$CA_URL = "https://${LOCAL_IP}"
\$ROOT_CERT_NAME = "root.crt"
\$DOWNLOAD_PATH = "\$env:TEMP\\\$ROOT_CERT_NAME"
\$CERT_STORE_LOCATION = "Cert:\LocalMachine\Root"

if (-not (Get-Command Invoke-WebRequest -ErrorAction SilentlyContinue)) {
    Write-Host "[ERR] Invoke-WebRequest not found, please use a modern version of PowerShell."
    exit 1
}

(New-Object System.Net.WebClient).DownloadFile("\$CA_URL/\$ROOT_CERT_NAME", "\$DOWNLOAD_PATH")
Write-Host "[INFO] Certificate downloaded to \$DOWNLOAD_PATH"

if (-Not (Test-Path \$DOWNLOAD_PATH)) {
    Write-Host "[ERR] Failed to download the certificate."
    exit 1
}

Import-Certificate -FilePath \$DOWNLOAD_PATH -CertStoreLocation \$CERT_STORE_LOCATION
Write-Host "[INFO] Root certificate installed successfully in Windows Trusted Root Store."
EOF

cp /etc/certificate-authority/install.sh /etc/certificate-authority/install.ps1 /var/www/html/files
systemctl -q enable --now nginx
systemctl -q restart nginx

cat > "${CA_DIR}/generate-certs.sh" << EOF
#!/bin/bash
set -e

CA_DIR="${CA_DIR}"
CERTS_DIR="${CERTS_DIR}"
ROOT_CA_PEM="${ROOT_CA_PEM}"
ROOT_CA_KEY="${ROOT_CA_KEY}"
OPENSSL_CONF="${OPENSSL_CONF}"

if test -f \${CA_DIR}/*.root.key; then
    printf "%s\\n" "[INFO] found existing CA"
    CA_NAME=\$(basename \$(printf "%s" \${CA_DIR}/*.root.key | awk -F '.root.key' '{print \$1}') )
fi

if ! test "\${1}" || ! test "\${2}"; then
    printf "%s\\n" \\
        "usage: \$(basename \$0) <common name without domain> <ip address for SAN>"
    exit 1
fi

CN="\${1}.\${CA_NAME}"
IP="\${2}.\${CA_NAME}"
CERT_PATH="\${CERTS_DIR}/\${CN}"
mkdir -p "\${CERT_PATH}" &>/dev/null

printf "%s\\n" "[INFO] generating certificate for \${CN}"

openssl \\
    req -x509 \\
    -newkey rsa:4096 \\
    -nodes \\
    -keyout "\${CERT_PATH}/\${CN}.key" \\
    -out "\${CERT_PATH}/\${CN}.crt" \\
    -sha256 \\
    -days 3650 \\
    -CA "\${ROOT_CA_PEM}" \\
    -CAkey "\${ROOT_CA_KEY}" \\
    -subj "/CN=\${CN}" \\
    -addext "subjectAltName = IP:\${2}, DNS:\${CN}" &>/dev/null

printf "%s\\n" \\
    "[INFO] certificate and key created" \\
    " - \${CERT_PATH}/\${CN}.crt" \\
    " - \${CERT_PATH}/\${CN}.key"
EOF
chmod +x "${CA_DIR}/generate-certs.sh"
echo "[INFO] certificate generation script created at ${CA_DIR}/generate-certs.sh"

qrencode -t png32 -s 10 -m 1 "https://${LOCAL_IP}/root.pem" -o "/var/www/html/files/${LOCAL_IP}-rootCA.png"
printf "%s\n" "[INFO] import the root CA to mobile devices and download install scripts from https://${LOCAL_IP}"
