#!/bin/bash


if test -z $1 || test -z $2 || test -z $3; then
    printf "%s\n" "usage: $(basename $0) <repo_list.txt> <gitea user> <git server domain e.g., gitea.local:4444>"
    exit 1
fi

if ! test -f gitea.txt; then
    printf "%s\n" "./gitea.txt with access token does not exist"
    exit 1
fi

token=$(cat gitea.txt)
migrate_api="https://${3}/api/v1/repos/migrate"

for i in $(cat $1); do
    if [[ $(curl -o /dev/null -s -w "%{http_code}" "${i}") != "404" ]]; then
        printf "%s" "[INFO] mirroring ${i} to ${gitea_url}... "

        clone_url="${i}"
        clone_name=$(basename $i)

        if [[ $(curl -o /dev/null -s -w "%{http_code}" "https://${gitea_url}/${2}/${clone_name}") != "200" ]]; then

            if curl -sSLk -o /dev/null -X 'POST' \
              $migrate_api \
              -H 'accept: application/json' \
              -d "token=${token}&mirror=true&clone_addr=${clone_url}&repo_name=${clone_name}"; then

                printf "%s\n" "done, sleeping 10"
                sleep 10
            else
                printf "%s\n" "error, problem with ${i}, sleeping 10"
                sleep 10
            fi
        else
            printf "%s\n" "exists on https://${gitea_url}/${2}/${clone_name}"
        fi
    else
        printf "%s\n" "[WARN] ${i} not found, skipping"
    fi
done
