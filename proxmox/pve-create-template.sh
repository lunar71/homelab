#!/bin/bash

usage() {
    printf "%s\n" \
        "usage: $(basename $0) -n <template name> -q <qcow image> -i <vm id>"
    exit 1
}

while getopts "n:q:i:h" opts; do
    case $opts in
        n) name="${OPTARG}";;
        q) qcow="${OPTARG}";;
        i) vmid="${OPTARG}";;
        h) usage
    esac
done

if test "${name}" && test "${qcow}" && test "${vmid}"; then
    if test -f "${qcow}"; then
        qm create "${vmid}" \
            --name "${name}" \
            --memory 4096 \
            --net0 virtio,bridge=vmbr0 \
            --bootdisk scsi0 \
            --boot order=scsi0

        qm importdisk "${vmid}" "${qcow}" local-lvm

        qm set "${vmid}" \
            -scsihw virtio-scsi-pci \
            -scsi0 "local-lvm:vm-"${vmid}"-disk-0"

        qm template "${vmid}"
    else
        printf "%s\n" "[ERR] ${qcow} does not exist"
        exit 1
    fi
else
    usage
fi
