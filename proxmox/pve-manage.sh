#!/bin/bash

trap 'set +x' EXIT

if ! command -v curl &>/dev/null; then
    printf "%s\n" "[ERR] curl not found"
    exit 1
fi

if ! command -v jq &>/dev/null; then
    printf "%s\n" "[ERR] jq not found"
    exit 1
fi

if ! command -v remote-viewer &>/dev/null; then
    printf "%s\n" "[ERR] remote-viewer not found"
    exit 1
fi

usage() {
    printf "%s\n" \
        "usage: $(basename $0) [-c <config file>] [-a <action> -i <vmid>] [-v] [-h]" \
        "" \
        "-c             config file, default \$HOME/.spice-connect/config.env" \
        "-a <action>    spice|list|reboot|reset|resume|shutdown|start|stop|suspend" \
        "-i <vmid>      target <vmid>" \
        "-v             enable bash shell trace" \
        "-h             print this help message and exit"
    exit 1
}

config="${HOME}/.spice-connect/config.env"
while getopts "c:a:i:vh" opts; do
    case $opts in
        c) config="${OPTARG}";;
        a) action="${OPTARG}";;
        i) vmid="${OPTARG}";;
        v) set -x;;
        h) usage;;
    esac
done

if test -f "${config}"; then
    source "${config}"
    if test -z "${PROXMOX_HOST}" && test -z "${PROXMOX_NODE}" && test -z "${PROXMOX_TOKEN}" && test -z "${PROXMOX_SECRET}"; then
        printf "%s\n" "[ERR] missing config values"
        exit 1
    fi
else
    printf "%s\n" "[ERR] ${config} does not exist"
    exit 1
fi

API="https://${PROXMOX_HOST}:8006/api2/json/nodes/${PROXMOX_NODE}/qemu"
SESSION_TOKEN="PVEAPIToken=${PROXMOX_TOKEN}=${PROXMOX_SECRET}"

case "${action}" in
    spice)
                if test "${vmid}"; then
                    tmpfile=$(mktemp)
                    curl -sSLk -H "Authorization: ${SESSION_TOKEN}" \
                        -X POST "${API}/${vmid}/spiceproxy" | \
                        jq -r '.data |
                    "[virt-viewer]
                    proxy=\(.proxy)
                    secure-attention=\(.["secure-attention"])
                    type=\(.type)
                    toggle-fullscreen=\(.["toggle-fullscreen"])
                    title=\(.title)
                    host=\(.host)
                    tls-port=\(.["tls-port"])
                    ca=\(.ca)
                    release-cursor=\(.["release-cursor"])
                    password=\(.password)
                    host-subject=\(.["host-subject"])
                    delete-this-file=\(.["delete-this-file"])"' > "${tmpfile}"

                    remote-viewer "${tmpfile}" &
                    #rm -rf "${tmpfile}"
                else
                    printf "%s\n" "[ERR] -i <vmid> missing"
                    usage
                fi
                ;;

    list)
                output=$(printf "%s\n" "vmid name status")
                output+=$'\n'
                output+="---- ---- ------"
                output+=$'\n'
                output+=$(curl -sSLk -H "Authorization: ${SESSION_TOKEN}" \
                    "${API}" | jq -r '.data[] | "\(.vmid) \(.name) \(.status)"' | grep -v 'template')
                printf "%s\n" "${output}" | column -t
                ;;

    reboot|reset|resume|shutdown|start|stop|suspend)
                if test "${vmid}"; then
                    curl -sSLk -H "Authorization: ${SESSION_TOKEN}" \
                        -X POST "${API}/${vmid}/status/${action}" | jq
                fi
                ;;

    *) usage    ;;
esac
